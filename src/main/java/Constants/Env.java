package Constants;

import CustomException.EnvironmentException;

import java.util.Properties;

/**
 * Created by anjukumari on 25/04/18
 */
public class Env {
    public static Properties sandbox;
    public static Properties integration;
    public static Properties hotfix;
    public static Properties automation;
    public static Properties envConfig;

    public static void getEnvProperty(String env) throws Exception{
        switch (env){
            case "sandbox":
                envConfig = sandbox;
                break;
            case "integration":
                envConfig = integration;
                break;
            case "hotfix":
                envConfig = hotfix;
                break;
            case "automation":
                envConfig = automation;
                break;
            default:
                throw new EnvironmentException("Environment does not exits");

        }

    }
}
