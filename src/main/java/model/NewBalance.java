package model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by anjukumari on 20/04/18
 */
public class NewBalance {

    @JsonProperty("phone")
    String phone;
    @JsonProperty("password")
    String password;
    @JsonProperty("token")
    String token;
    @JsonProperty("wallet")
    String wallet;
    @JsonProperty("ppbl")
    String ppbl;
    @JsonProperty("digitalcredit")
    String digitalcredit;
    @JsonProperty("environemnt")
    String environment;

    public String getEnvironment() {
        return environment;
    }

    public NewBalance setEnvironment(String environment) {
        this.environment = environment;
        return this;
    }
    public String getPhone() {
        return phone;
    }
    public NewBalance setPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public String getToken() {
        return token;
    }

    public NewBalance setToken(String token) {
        this.token = token;
        return this;
    }
    @JsonProperty("wallet")
    public String getWallet() {
        return wallet;
    }
    @JsonProperty("wallet")
    public NewBalance setWallet(String wallet) {
        this.wallet = wallet;
        return this;
    }

    public String getPpbl() {
        return ppbl;
    }

    public NewBalance setPpbl(String ppbl) {
        this.ppbl = ppbl;
        return this;
    }

    public String getDigitalcredit() {
        return digitalcredit;
    }

    public NewBalance setDigitalcredit(String digitalcredit) {
        this.digitalcredit = digitalcredit;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public NewBalance setPassword(String password) {
        this.password = password;
        return this;
    }

    @Override
    public String toString() {
        return "NewBalance{" +
                "phone='" + phone + '\'' +
                ", password='" + password + '\'' +
                ", token='" + token + '\'' +
                ", wallet='" + wallet + '\'' +
                ", ppbl='" + ppbl + '\'' +
                ", digitalcredit='" + digitalcredit + '\'' +
                ", environment='" + environment + '\'' +
                '}';
    }
}
