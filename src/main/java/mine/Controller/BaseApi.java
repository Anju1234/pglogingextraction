package mine.Controller;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.ErrorLoggingFilter;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import static io.restassured.RestAssured.given;

/**
 * Created by anjukumari on 25/04/18
 */
public class BaseApi {

    private RequestSpecBuilder requestSpecBuiler = new RequestSpecBuilder();
    private RestMethodType restMethodType;

    public RestMethodType getRestMethodType() {
        return restMethodType;
    }

    public BaseApi setRestMethodType(RestMethodType restMethodType) {
        this.restMethodType = restMethodType;
        return this;
    }

    public RequestSpecBuilder getRequestSpecBuilder() {
        return requestSpecBuiler;
    }

    public Response execute(){
        Response response;

        RequestSpecification requestSepc = requestSpecBuiler
                .addFilter(new RequestLoggingFilter())
                .addFilter(new ResponseLoggingFilter())
                .addFilter(new ErrorLoggingFilter())
                .build();
        switch(restMethodType){
            case GET:{
                response  = given().log().all().spec(requestSepc).get();
                break;
            }
            case PUT:{
                response  = given().spec(requestSepc).put();
                break;
            }
            case POST:{
                response  = given().log().all().spec(requestSepc).post();
                break;
            }
            case PATCH:{
                response  = given().spec(requestSepc).patch();
                break;
            }
            case DELETE:{
                response  = given().spec(requestSepc).delete();
                break;
            }
            default:{
                throw new RuntimeException("Method type not found");
            }
        }
        return response;
    }

    public  enum RestMethodType{
        POST, GET, PUT, DELETE, PATCH

    }

}
