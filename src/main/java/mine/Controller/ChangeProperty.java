package mine.Controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by anjukumari on 11/04/18
 */


@RestController
public class ChangeProperty {
    @RequestMapping(
            consumes = {MediaType.ALL_VALUE},
            path = "/",
            method = RequestMethod.POST
    )
    public ResponseEntity<?> test(){
        System.out.println("hello");
        return new ResponseEntity(HttpStatus.OK);
    }
}
