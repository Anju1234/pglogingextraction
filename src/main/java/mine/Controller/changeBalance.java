package mine.Controller;

import Constants.Env;
import model.NewBalance;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import utils.OauthUtil;
import utils.WalletUtil;

import static modules.Wallet.modifyBalance;

/**
 * Created by anjukumari on 20/04/18
 */
@RestController
public class changeBalance {

    @RequestMapping(
            consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE},
            path = "/changeBalance",
            method = RequestMethod.POST,
            produces = MediaType.ALL_VALUE
    )
    public ResponseEntity<?> BalanceModification(@ModelAttribute NewBalance newBalance) throws Exception{
        String ssoToken;
        System.out.println(">>>>"+newBalance.toString());
        Env.getEnvProperty(newBalance.getEnvironment());
        ssoToken = newBalance.getToken();
        System.out.println(newBalance.getPhone());
        //if(null != ssoToken ) {
            if (ssoToken.isEmpty() && !newBalance.getPhone().isEmpty()) {
                ssoToken = OauthUtil.getSSOToken(Env.envConfig.getProperty("authhost"), newBalance.getPhone(), newBalance.getPassword());
                newBalance.setToken(ssoToken);
            }
        //}
        resetWalletBalance(newBalance);
        resetPaymentBankBalance(newBalance);
        digitalCardBalance(newBalance);
        Double walletUpdatedBalance = WalletUtil.getWalletBalance(Env.envConfig.getProperty("wallethost"),ssoToken);
        String digitalUpdatedBalance = "sdfsdf";
        String ppblUpdatedBalance = "sdd";
        String responseJson = "{ \"wallet_balance\":\""+walletUpdatedBalance+"\", \"digital_credit\":\""+digitalUpdatedBalance+"\", \"payment_bank\":\""+ppblUpdatedBalance+"}";
        return new ResponseEntity(responseJson,HttpStatus.OK);
    }



    public void resetWalletBalance(NewBalance newBalance){
        String newWalletBalance = newBalance.getWallet();
        if(newWalletBalance != null){
            modifyBalance(Env.envConfig.getProperty("wallethost"), newBalance.getPhone(),newBalance.getToken(), Double.parseDouble(newWalletBalance));
        }
    }

    public void resetPaymentBankBalance(NewBalance newBalance){
        String newPPBLBalance = newBalance.getPpbl();
        if(newPPBLBalance != null){
        }
    }

    public void digitalCardBalance(NewBalance newBalance){
        String newDigitalBalance = newBalance.getDigitalcredit();
        if(newDigitalBalance != null){

        }
    }

}
