package mine.Controller;

import com.sun.xml.internal.xsom.impl.Ref;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by anjukumari on 03/05/18
 */

@RestController
public class TxnDetail {

    @RequestMapping(
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            path = "/txnDetail",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public void txnDetail(){

    }

}
