package mine.Controller;
import Constants.Env;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import utils.PropertyUtil;

import java.util.HashMap;
import java.util.Properties;


/**
 * Created by anjukumari on 11/04/18
 */

@SpringBootApplication
public class Runner{

    public Runner() {
        PropertyUtil propUtil = new PropertyUtil();
        Env.sandbox = propUtil.loadProperty("sandboxConfig.properties");
        Env.integration = propUtil.loadProperty("integrationConfig.properties");
        Env.hotfix = propUtil.loadProperty("hotfixConfig.properties");
        Env.automation = propUtil.loadProperty("automationConfig.properties");
    }
    public static void main(String[] args) {
        SpringApplication.run(Runner.class, args);
    }

   /*public static void main(String[] args){
       PropertyUtil propUtil = new PropertyUtil();
       Env.sandbox = propUtil.loadProperty("sandboxConfig.properties");
   }
*/

}

