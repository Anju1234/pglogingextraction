package CustomException;

/**
 * Created by anjukumari on 26/04/18
 */
public class EnvironmentException extends Exception {
    public EnvironmentException(String message){
        super(message);
    }
}
