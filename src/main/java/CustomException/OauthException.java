package CustomException;

/**
 * Created by anjukumari on 26/04/18
 */
public class OauthException extends Exception{

    public OauthException(String message){
        super(message);
    }
}
