package modules;

import Constants.Env;
import mine.Controller.BaseApi;
import model.NewBalance;
import org.assertj.core.api.Assertions;
import utils.WalletUtil;

import static utils.WalletUtil.getWalletBalance;

/**
 * Created by anjukumari on 25/04/18
 */
public class Wallet {



    public void checkBalance(String phone){

    }

    public void addBalance(NewBalance newBalance){


    }


    public static void modifyBalance(String walletBaseUri, String mobileNumber, String walletToken, Double amountToBeRetainedInWallet) {
        double currentBalance = getWalletBalance(walletBaseUri, walletToken);
        double amount = amountToBeRetainedInWallet - currentBalance;
        UpdatingBalance:
        {
            if (amount > 0 && amount < 1) {
                WalletUtil.addAmount(walletBaseUri, walletToken, 1 + amount, mobileNumber);
                WalletUtil.withdrawAmount(walletBaseUri, walletToken, 1.00);
            } else if (amount >= 1) {
                WalletUtil.addAmount(walletBaseUri,walletToken, amount, mobileNumber);
            } else if (amount < 0) {
                WalletUtil.withdrawAmount(walletBaseUri, walletToken, -amount);
            }
        }
        if (amount != 0.00) {
            currentBalance = getWalletBalance(walletBaseUri, walletToken);
        }
        Assertions.assertThat(currentBalance).isEqualTo(amountToBeRetainedInWallet).as("Wallet balance could not be modified successfully");
    }


}
