package utils;

import Constants.Env;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import mine.Controller.BaseApi;
import org.assertj.core.api.Assertions;

/**
 * Created by anjukumari on 26/04/18
 */
public class WalletUtil extends BaseApi{
    public static final String GET_BALANCE = "/wallet-web/checkBalance";
    public static final String WITHDRAW = "/wallet-web/withdraw";
    public static final String ADD_MONEY_CASH = "/wallet-web/addMoneyCash";
    public static final String BLOCK_AMOUNT = "/wallet-web/blockDisputeTxn";

    public static double getWalletBalance(String walletBaseUri, String ssoToken) {
        BaseApi baseAPI = new BaseApi();
        APIBuilder:
        {
            baseAPI.setRestMethodType(RestMethodType.GET);
            RequestSpecBuilder requestSpecBuilder = baseAPI.getRequestSpecBuilder();
            requestSpecBuilder.setContentType(ContentType.JSON);
            requestSpecBuilder.addHeader("ssotoken", ssoToken);
            requestSpecBuilder.addHeader("is_admin", "false");
            requestSpecBuilder.addHeader("tokentype", "OAUTH");
            requestSpecBuilder.setBaseUri(walletBaseUri);
            requestSpecBuilder.setBasePath(GET_BALANCE);
        }
        Response response = baseAPI.execute();
        JsonPath jsonPath = response.jsonPath();
        double walletBalance = Double.parseDouble(jsonPath.get("response.amount").toString());
        return walletBalance;
    }

    public static Response addAmount(String walletBaseUri, String ssotoken, Double amount, String mobileNumber){
        BaseApi baseApi = new BaseApi();
        String merchantGuid = "125FD26C-4D98-11E2-B20C-E89A8FF309EA";
        String walletGuid = "1BE2E64B-8596-4553-BC30-5687B29EF7A8";
        String merchantOrderId = Long.toString(System.currentTimeMillis());
        String body = "{\"request\":{\"merchantGuid\":\"" + merchantGuid + "\",\"merchantOrderId\":\"" + merchantOrderId + "\",\"walletName\":\"AutomationTesting\",\"walletGuid\":\"" + walletGuid + "\",\"payeeEmailId\":\"sandeep.kumar@paytm.com\",\"payeePhoneNumber\":\"" + mobileNumber + "\",\"amount\":\"" + amount + "\",\"currencyCode\":\"INR\",\"pgTxnId\":\"" + merchantOrderId + "\",\"applyVerifiedUser\":\"0\",\"appliedToNewUsers\":\"Y\"},\"metadata\":\"AutomationTesting\",\"ipAddress\":\"127.0.0.1\",\"platformName\":\"PayTM\",\"operationType\":\"ADD_MONEY_CASH_TXN\"}";
        APIBuilder:{
            baseApi.setRestMethodType(RestMethodType.POST);
            RequestSpecBuilder specBuilder = baseApi.getRequestSpecBuilder();
            specBuilder.setContentType(ContentType.JSON);
            specBuilder.addHeader("ssotoken", ssotoken);
            specBuilder.addHeader("tokentype",  "OAUTH");
            specBuilder.addHeader("is_admin", "false");
            specBuilder.setBody(body);
        }
        Response response = baseApi.execute();
        JsonPath jsonPath = response.jsonPath();
        Assertions.assertThat((String) jsonPath.get("statusCode")).as("Amt could not be added to wallet.").isEqualToIgnoringCase("SUCCESS");
        return response;
    }

    public static Response withdrawAmount(String walletBaseUri, String ssoToken, Double amount){
        BaseApi baseApi = new BaseApi();
        String merchantGuid = "125FD26C-4D98-11E2-B20C-E89A8FF309EA";
        String merchantOrderId = Long.toString(System.currentTimeMillis());
        String body = "{ \"request\": { \"skipRefill\": true, \"totalAmount\": \"" + amount + "\", \"currencyCode\": \"INR\", \"merchantGuid\": \"" + merchantGuid + "\", \"industryType\":\"PVT_LTD\", \"merchantOrderId\": \"" + merchantOrderId + "\", \"pgTxnId\": \"" + merchantOrderId + "\"}, \"platformName\": \"PayTM\", \"ipAddress\": \"192.168.40.11\", \"operationType\": \"WITHDRAW_MONEY\" }";
        RequestSpecBuilder specBuilder = baseApi.getRequestSpecBuilder();
        APIBuilder:{
            specBuilder.setContentType(ContentType.JSON);
            specBuilder.setBaseUri(walletBaseUri);
            specBuilder.addParam("is_admin", "false");
            specBuilder.addParam("ssotoken", ssoToken);
            specBuilder.setBody(body);
        }
        Response response = baseApi.execute();
        return  response;
    }

}
