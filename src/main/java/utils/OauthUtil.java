package utils;

import CustomException.OauthException;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import mine.Controller.BaseApi;

/**
 * Created by anjukumari on 26/04/18
 */
public class OauthUtil extends BaseApi{
    private static final String AUTHORIZE = "/oauth2/authorize";
    private static final String TOKEN = "/oauth2/token";
    private static final String USERTOKENS = "/oauth2/usertokens";
    private static final String RESOURCE_USER = "resource/user";

    public static synchronized String getSSOToken(String authBaseUri, String mobile, String password) throws OauthException {
        BaseApi baseApi = new BaseApi();
        String authorization = "Basic cGF5dG0tcGctY2xpZW50LXN0YWdpbmc6YTc0MjZiZTAtYTJkZC00N2NmLWExODEtYjM3YzgwMWYzNGM2";
        String body = "response_type=code&client_id=paytm-pg-client-staging&scope=paytm&username=" + mobile + "&password=" + password + "&do_not_redirect=true";
        APIBuilder:
        {
            baseApi.setRestMethodType(RestMethodType.POST);
            RequestSpecBuilder requestSpecBuilder = baseApi.getRequestSpecBuilder();
            requestSpecBuilder.setContentType(ContentType.URLENC);
            requestSpecBuilder.addHeader("authorization", authorization);
            requestSpecBuilder.setBaseUri(authBaseUri);
            requestSpecBuilder.setBasePath(AUTHORIZE);
            requestSpecBuilder.setBody(body);
        }
        Response response = baseApi.execute();
        System.out.println("oauth2/authorize response: "+response.asString());
        String code = response.jsonPath().get("code");
        if (code != null && !code.isEmpty()) {
            BaseApi baseAPIToken = new BaseApi();
            APIBuilder:
            {
                baseAPIToken.setRestMethodType(RestMethodType.POST);
                RequestSpecBuilder requestSpecBuilder = baseAPIToken.getRequestSpecBuilder();
                requestSpecBuilder.setContentType(ContentType.URLENC);
                requestSpecBuilder.addHeader("authorization", authorization);
                requestSpecBuilder.setBaseUri(authBaseUri);
                requestSpecBuilder.setBasePath(TOKEN);
                requestSpecBuilder.addFormParam("grant_type", "authorization_code");
                requestSpecBuilder.addFormParam("code", code);
                requestSpecBuilder.addFormParam("scope", "paytm");

            }
            Response responseToken = baseAPIToken.execute();
            System.out.println("oauthtoken response::::: "+responseToken.asString());
            String accessToken = responseToken.jsonPath().get("access_token");
            if (accessToken == null || accessToken.isEmpty()) {
                throw new OauthException("SSO Token is either null or empty: " + accessToken);
            }
            return accessToken;
        } else {
            throw new OauthException("Code is either null or empty: " + code);
        }
    }
}
