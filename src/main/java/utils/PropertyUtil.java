package utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by anjukumari on 25/04/18
 */
public class PropertyUtil {

    public  Properties loadProperty(String... filePath) {
        Properties property =null;
        try {
            for (int i = 0; i < filePath.length; i++) {
                InputStream input = new FileInputStream(new File(filePath[i]));
                property = new Properties();
                property.load(input);
            }
        } catch (Exception e) {
                throw new RuntimeException("Not able to load property");
        }
        return property;
    }
    public  Properties loadProperty(String fileName) {
        Properties property = null;
        try {
                InputStream input = getClass().getClassLoader().getResourceAsStream(fileName);
                property = new Properties();
                property.load(input);
                return property;
        } catch (Exception e) {

        }
        return property;
    }
}
