package utils;

/**
 * Created by anjukumari on 05/04/18
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpATTRS;

public class SftpTransferHelper {

    private static final Logger logger = LoggerFactory.getLogger(SftpTransferHelper.class);
    public static final int DEFAULT_SFTPPORT = 22;

    public static boolean upload(String localFilePath, String SFTPHOST, String SFTPUSER, String SFTPPASS, String SFTPWORKINGDIR) {
        return upload(localFilePath, SFTPHOST, SFTPUSER, SFTPPASS, SFTPWORKINGDIR, DEFAULT_SFTPPORT, false);
    }

    public static boolean upload(String localFilePath, String SFTPHOST, String SFTPUSER, String SFTPPASS, String SFTPWORKINGDIR, int SFTPPORT) {
        return upload(localFilePath, SFTPHOST, SFTPUSER, SFTPPASS, SFTPWORKINGDIR, SFTPPORT, false);
    }

    public static boolean upload(String localFilePath, String SFTPHOST, String SFTPUSER, String SFTPWORKINGDIR) {
        return upload(localFilePath, SFTPHOST, SFTPUSER, null, SFTPWORKINGDIR, DEFAULT_SFTPPORT, false);
    }

    public static boolean upload(String localFilePath, String SFTPHOST, String SFTPUSER, String SFTPWORKINGDIR, int SFTPPORT) {
        return upload(localFilePath, SFTPHOST, SFTPUSER, null, SFTPWORKINGDIR, SFTPPORT, false);
    }

    public static boolean uploadUsingKey(String localFilePath, String SFTPHOST, String SFTPUSER, String SFTPKEYPATH, String SFTPWORKINGDIR) {
        return upload(localFilePath, SFTPHOST, SFTPUSER, SFTPKEYPATH, SFTPWORKINGDIR, DEFAULT_SFTPPORT, true);
    }

    public static boolean uploadUsingPassword(String localFilePath, String SFTPHOST, String SFTPUSER, String SFTPKEYPATH, String SFTPWORKINGDIR) {
        return upload(localFilePath, SFTPHOST, SFTPUSER, SFTPKEYPATH, SFTPWORKINGDIR, DEFAULT_SFTPPORT, false);
    }

    public static boolean uploadUsingKey(String localFilePath, String SFTPHOST, String SFTPUSER, String SFTPKEYPATH, String SFTPWORKINGDIR, int SFTPPORT) {
        return upload(localFilePath, SFTPHOST, SFTPUSER, SFTPKEYPATH, SFTPWORKINGDIR, SFTPPORT, true);
    }

    public static boolean download(String fileName, String localDestinationPath, String SFTPHOST, String SFTPUSER, String SFTPPASS, String SFTPSOURCEPATH, int SFTPPORT) {
        return download(fileName, localDestinationPath, SFTPHOST, SFTPUSER, SFTPPASS, SFTPSOURCEPATH, SFTPPORT, false);
    }

    public static boolean upload(String localFilePath, String SFTPHOST, String SFTPUSER, String SFTPPASS, String SFTPWORKINGDIR, int SFTPPORT, boolean connectUsingKey) {
        boolean uploaded = false;
        Session session = null;
        Channel channel = null;
        ChannelSftp channelSftp = null;
        logger.info("localFilePath:[" + localFilePath + "] SFTPHOST:[" + SFTPHOST + "] SFTPUSER:[" + SFTPUSER + "] SFTPWORKINGDIR:["
                + SFTPWORKINGDIR + "]");
        logger.info("preparing the host information for sftp.");
        try {
            JSch jsch = new JSch();
            if (connectUsingKey) {
                logger.info("connecting using private key path.");
                jsch.addIdentity(SFTPPASS);
            }
            session = jsch.getSession(SFTPUSER, SFTPHOST, SFTPPORT);
            if (!connectUsingKey) {
                logger.info("connecting using password.");
                session.setPassword(SFTPPASS);
            }
            java.util.Properties config = new java.util.Properties();
            config.put("StrictHostKeyChecking", "no");
            session.setConfig(config);
            session.connect();
            logger.info("Host connected.");
            channel = session.openChannel("sftp");
            channel.connect();
            logger.info("sftp channel opened and connected.");
            channelSftp = (ChannelSftp) channel;

            //SftpATTRS attrs;
            try {
                SftpATTRS attrs = channelSftp.stat(SFTPWORKINGDIR);
            }catch (Exception e) {
                channelSftp.mkdir(SFTPWORKINGDIR);
            }
            channelSftp.cd(SFTPWORKINGDIR);
            File f = new File(localFilePath);
            channelSftp.put(new FileInputStream(f), f.getName());
            logger.info("File transfered successfully to host.");
            uploaded = true;
        } catch (Exception ex) {
            logger.info("Exception found while tranfering the file.", ex);
        } finally {
            if (channelSftp != null) {
                channelSftp.exit();
            }
            if (channel != null) {
                channel.disconnect();
            }
            if (session != null) {
                session.disconnect();
            }
            logger.info("sftp channel closed and disconnected.");
        }
        return uploaded;
    }

    public static boolean download(String fileName, String localDestinationPath, String SFTPHOST, String SFTPUSER, String SFTPPASS, String SFTPSOURCEPATH, int SFTPPORT, boolean connectUsingKey) {
        boolean downloaded = false;
        Session session = null;
        Channel channel = null;
        ChannelSftp channelSftp = null;
        logger.info("file:[" + fileName +"] localDestinationPath:[" + localDestinationPath + "] SFTPHOST:[" + SFTPHOST + "] SFTPUSER:[" + SFTPUSER + "] SFTPSOURCEPATH:["
                + SFTPSOURCEPATH + "]");
        logger.info("preparing the host information for sftp.");
        try {
            JSch jsch = new JSch();
            if (connectUsingKey) {
                logger.info("connecting using private key path.");
                jsch.addIdentity(SFTPPASS);
            }
            session = jsch.getSession(SFTPUSER, SFTPHOST, SFTPPORT);
            if (!connectUsingKey) {
                logger.info("connecting using password.");
                session.setPassword(SFTPPASS);
            }
            java.util.Properties config = new java.util.Properties();
            config.put("StrictHostKeyChecking", "no");
            session.setConfig(config);
            session.connect();
            logger.info("Host connected.");
            channel = session.openChannel("sftp");
            channel.connect();
            logger.info("sftp channel opened and connected.");
            channelSftp = (ChannelSftp) channel;
            channelSftp.cd(SFTPSOURCEPATH);
            File localFile = new File(localDestinationPath+fileName);
            logger.info("initiating file copy...");
            FileOutputStream fos = new FileOutputStream(localFile);
            channelSftp.get(SFTPSOURCEPATH+fileName, fos);
            fos.close();
            logger.info("File downloaded successfully from host.");
            downloaded = true;
        } catch (Exception ex) {
            logger.info("Exception found while tranfering the file.", ex);
        } finally {
            if (channelSftp != null) {
                channelSftp.exit();
            }
            if (channel != null) {
                channel.disconnect();
            }
            if (session != null) {
                session.disconnect();
            }
            logger.info("sftp channel closed and disconnected.");
        }
        return downloaded;
    }

    public static void main(String[] args) {

        String SFTPHOST = "115.124.16.69";
        int SFTPPORT = 22;
        String SFTPUSER = "test_upload_india";
        String SFTPPASS = "123456";
        String SFTPWORKINGDIR = "/upload/wallet_reports/";
        String localFilePath = "/tmp/txn/csv/";
		/*
		 * send("/home/ashutosh/Downloads/Withdraw_Txn_2014_10_08_04_15_00.csv.zip"
		 * );
		 */
        download("6082.zip", localFilePath, SFTPHOST, SFTPUSER, SFTPPASS, SFTPWORKINGDIR, SFTPPORT);

        //upload(localFilePath, SFTPHOST, SFTPUSER, SFTPPASS, SFTPWORKINGDIR);
    }
}

